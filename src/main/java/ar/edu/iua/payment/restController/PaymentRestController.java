package ar.edu.iua.payment.restController;

import ar.edu.iua.payment.business.PaymentBusinessI;
import ar.edu.iua.payment.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Constants.PAYMENT_URL)
public class PaymentRestController {

    @Autowired
    private PaymentBusinessI paymentBusiness;
    //instancio la config de rabbit para mandar un mensaje
    @Autowired
    private RabbitMQSender rabbitMqSender;
    @GetMapping(value = {"/{id}"})
    public ResponseEntity<PaymentGET> Payment (@PathVariable int id){

        try{
            return new ResponseEntity<PaymentGET>(paymentBusiness.getPaymentById(id), HttpStatus.OK);

        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(value = {"", "/"})
    public ResponseEntity<PaymentStatusDto> generatePayment(@RequestBody PaymentDto paymentDto){

        try {
            System.out.println("PAYMENT" + paymentDto);
            PaymentStatusDto paymentStatusDto = paymentBusiness.generatePayment(paymentDto);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("location", "/paymentDto" + paymentStatusDto.getPaymentId());
            //si el status del pago  es "APROBADO" ennvio el ID del pago a la cola
            if(paymentStatusDto.getStatus().equals("APROBADA")){
                rabbitMqSender.send(paymentStatusDto.getPaymentId());
                System.out.println("MENSAJE ENVIADO ");
            }
            return new ResponseEntity<>(paymentStatusDto, responseHeaders, HttpStatus.CREATED);

        }catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

}
