package ar.edu.iua.payment.model;

public class PaymentStatusDto {

    private int paymentId;
    private String status;
    private String approvalCode;

    public PaymentStatusDto(int paymentId, String status, String approvalCode) {
        this.paymentId = paymentId;
        this.status = status;
        this.approvalCode = approvalCode;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }
}
