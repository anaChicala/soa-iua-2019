package ar.edu.iua.payment.model;

public class tarjeta {

    private String numero;
    private String vencimiento;
    private String cvc;

    public tarjeta(String numero, String vencimiento, String cvc) {
        this.numero = numero;
        this.vencimiento = vencimiento;
        this.cvc = cvc;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(String vencimiento) {
        this.vencimiento = vencimiento;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }
}
