package ar.edu.iua.payment.business.implementation;

import ar.edu.iua.payment.business.PaymentBusinessI;
import ar.edu.iua.payment.model.*;
import ar.edu.iua.payment.model.persistance.PaymentRepository;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Service;

import java.io.FileReader;
// import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class PaymentBusiness implements PaymentBusinessI {

    @Autowired
    PaymentRepository paymentRepository;

    @Override
    public PaymentStatusDto generatePayment(PaymentDto paymentDto) {
        System.out.println(paymentDto.getAmount() + " " + paymentDto.getCreditCard().getNumber());
        Payment payment = new Payment();
        tarjeta tarjeta = new tarjeta(paymentDto.getCreditCard().getNumber(), paymentDto.getCreditCard().getExpirationDate(), paymentDto.getCreditCard().getCvc());
        AuthorizationDto authorizationDto = new AuthorizationDto(tarjeta, paymentDto.getAmount());

        Gson gson = new Gson();
        String json = gson.toJson(authorizationDto);
        System.out.println(json);
        HttpClient httpClient = HttpClientBuilder.create().build();
        String responseString = "";

        try {

            HttpPost request = new HttpPost("https://iua-service.herokuapp.com/autorizar");
            StringEntity params =new StringEntity(json);
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            responseString = new BasicResponseHandler().handleResponse(response);


        }catch (Exception ex) {

            //handle exception here

        } finally {
            //Deprecated
            //httpClient.getConnectionManager().shutdown();
        }

        AuthorizationAnswerDto authorizationAnswerDto = new Gson().fromJson(responseString, AuthorizationAnswerDto.class);
        /*
        Logica de llamar a la api de tarjeta para ver si el pago esta aprobado o no
         */
        String cardReturnCode = authorizationAnswerDto.getAutorizacion();
        String cardReturnStatus = authorizationAnswerDto.getEstado();
        String cardReturnApprovalCode = authorizationAnswerDto.getCodigo();

        //  Build the payment to store in database

        if(cardReturnStatus.equals("APROBADA")){
            payment.setStatus("APROBADA");
        }else if(cardReturnStatus.equals("RECHAZADA")){
            payment.setStatus("RECHAZADA");
        }

        payment.setAmount(paymentDto.getAmount());
        payment.setOrderId(paymentDto.getOrderId());

        Date date = new Date();
        payment.setPaymentDate(date);
        Payment returnedPayment = paymentRepository.save(payment);




        return new PaymentStatusDto(returnedPayment.getPaymentId(), returnedPayment.getStatus(), cardReturnApprovalCode);

    }

    public PaymentGET getPaymentById(int id){
        Optional optional = paymentRepository.findById(id);
        Payment p = (Payment)optional.get();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date =simpleDateFormat.format(p.getPaymentDate());

        PaymentGET paymentGET = new PaymentGET(p.getOrderId(), p.getAmount(), p.getStatus(), date, p.getPaymentId()) ;

        return paymentGET;
    }
}
