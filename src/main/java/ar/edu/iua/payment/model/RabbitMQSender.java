package ar.edu.iua.payment.model;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class RabbitMQSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("test-exchange")
    private String exchange;

    @Value("testKey")
    private String routingkey;

    public void send(int paymentId) {
        rabbitTemplate.convertAndSend(exchange, routingkey, paymentId);
        System.out.println("Send msg = " + paymentId);

    }
}