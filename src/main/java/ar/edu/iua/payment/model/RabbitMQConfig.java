package ar.edu.iua.payment.model;


import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.remoting.client.AmqpProxyFactoryBean;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    //Nombre de la queue
    @Value("test-queue")
    String queueName;
    //nombre del exchange
    @Value("test-exchange")
    String exchangeName;
    //routingkey para poder enviar los mensajes
    @Value("testKey")
    private String routingkey;
    //CREA LA QUEUE
    @Bean
    Queue queue() {
        return new Queue(queueName);
    }
//esto se usa para conectarte a una queue remota de forma que parezca que esta corriendo localmente
    /*  @Bean
      AmqpProxyFactoryBean amqpFactoryBean(AmqpTemplate amqpTemplate) {
          AmqpProxyFactoryBean factoryBean = new AmqpProxyFactoryBean();
          factoryBean.setServiceInterface(CabBookingService.class);
          factoryBean.setAmqpTemplate(amqpTemplate);
          return factoryBean;
      }
  */
    //CREA UN EXCHANGE DE TIPO DIRECTO
    @Bean
    Exchange directExchange(Queue someQueue) {
        DirectExchange exchange = new DirectExchange(exchangeName);
        BindingBuilder.bind(someQueue).to(exchange).with(routingkey);
        return exchange;
    }
//USA ESTE TEMPLATE PARA PODER ENVIAR EL MENSAJE en formato json

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}