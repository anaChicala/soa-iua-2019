package ar.edu.iua.payment.business;

import ar.edu.iua.payment.model.Payment;
import ar.edu.iua.payment.model.PaymentDto;
import ar.edu.iua.payment.model.PaymentGET;
import ar.edu.iua.payment.model.PaymentStatusDto;

public interface PaymentBusinessI {

    PaymentStatusDto generatePayment (PaymentDto paymentDto);
    PaymentGET getPaymentById(int id);
}
