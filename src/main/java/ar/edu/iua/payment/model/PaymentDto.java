package ar.edu.iua.payment.model;

public class PaymentDto {

    private int amount;
    private int orderId;
    private CreditcardDto creditCard;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public CreditcardDto getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditcardDto creditCard) {
        this.creditCard = creditCard;
    }

    @Override
    public String toString() {
        return "PaymentDto{" +
                "amount=" + amount +
                ", orderId=" + orderId +
                ", creditCard=" + creditCard +
                '}';
    }
}
