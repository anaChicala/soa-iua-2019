package ar.edu.iua.payment.model;

public class AuthorizationDto {

    private tarjeta tarjeta;
    private double monto;

    public AuthorizationDto(tarjeta tarjeta, double monto) {
        this.tarjeta = tarjeta;
        this.monto = monto;
    }

    public tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }
}
