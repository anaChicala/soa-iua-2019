package ar.edu.iua.payment.restController;

public class Constants {

    public static final String API_URL = "/api";
    public static final String URL_API_VERSION= "/v1";

    public static final String BASE_URL = API_URL + URL_API_VERSION;
    public static final String PAYMENT_URL = BASE_URL + "/payment";
}
